import * as React from 'react';
import SignInUpPage from '../signInUpPage';
import {isSignedIn} from '../../services/authService';
import Fight from '../fight';
import SignOut from '../signOut';
import Arena from "../arena";
import WinnerModal from "../winnerModal";

class StartScreen extends React.Component {
    state = {
        isSignedIn: false,
        isFightStarted: false,
        fighter1: null,
        fighter2: null,
        isWinner: false
    };

    componentDidMount() {
        this.setIsLoggedIn(isSignedIn());
    }

    setIsLoggedIn = (isSignedIn) => {
        this.setState({isSignedIn});
    };

    setFightStarted = (isFightStarted) => {
        this.setState({isFightStarted});
    };

    setSelectedFighters = ([fighter1, fighter2]) => {
        this.setState({...this.state, fighter1, fighter2});
    };

    setWinner = (winner) => {
        const state = {...this.state};
        this.setState({...state, isWinner: true, winner});
    };

    setNewGame = () => {
        const state = {...this.state};
        this.setState({
            ...state,
            isFightStarted: false,
            fighter1: null,
            fighter2: null,
            isWinner: false,
            winner: null
        });
    };

    render() {
        const {isSignedIn, isFightStarted, isWinner} = this.state;

        if (!isSignedIn) {
            return <SignInUpPage setIsLoggedIn={this.setIsLoggedIn}/>
        }

        return (
            <>
                {!isFightStarted &&
                <Fight onStartFight={this.setFightStarted} setSelectedFighters={this.setSelectedFighters}/>}
                <SignOut isSignedIn={isSignedIn} onSignOut={() => this.setIsLoggedIn(false)}/>
                {isFightStarted &&
                <Arena fighters={[this.state.fighter1, this.state.fighter2]} setWinner={this.setWinner}/>}
                {isWinner && <WinnerModal winner={this.state.winner} setNewGame={this.setNewGame}/>}
            </>
        );
    }
}

export default StartScreen;
