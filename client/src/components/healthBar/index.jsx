import React from 'react';

import {LinearProgress, withStyles} from '@material-ui/core';

import './healthBar.css';

const StyledLinearProgress = withStyles({
    root: {
        height: 25,
        backgroundColor: 'transparent'
    },
    barColorPrimary: {
        backgroundColor: '#ff9800'
    }
})(LinearProgress);

const HealthBar = ({ progress = 100 }) => {
    return (
        <>
            <StyledLinearProgress variant="determinate" value={ progress < 0 ? 0 : progress } />
        </>
    );
};

export default HealthBar;
