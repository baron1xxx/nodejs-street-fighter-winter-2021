import React from 'react';
import BattleFighter from "../battleFighter";

import './battleField.css';


const BattleField = () => {
    const srcFighter1 = 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif';
    const srcFighter2 = 'https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif';
    return (
        <div className="battlefield">
            <BattleFighter fighterImg={srcFighter1} position="left" />
            <BattleFighter fighterImg={srcFighter2} position="right" />
        </div>
    );
};

export default BattleField;
