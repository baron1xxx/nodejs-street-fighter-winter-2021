import React from 'react';
import HealthIndicator from "../healthIndicator";

import './fighterIndicator.css';

const FighterIndicator = ({fighter, currentDamageHealth}) => {
    return (
        <div className="fighter-indicator">
            <span className="fighter-name">{fighter.name}</span>
            <HealthIndicator fighter={fighter} currentDamageHealth={currentDamageHealth}/>
        </div>
    );
};

export default FighterIndicator;
