import React, {useEffect, useState} from 'react';
import FightStatus from "../fightStatus";
import BattleField from "../battleField";
import {controls} from "../../constants/controls";
import {getDamage} from "../../services/fight";

import './arena.css';


const {
    PlayerOneBlock,
    PlayerTwoBlock,
    PlayerOneAttack,
    PlayerTwoAttack,
} = controls;

const Arena = ({fighters, setWinner}) => {
    const [fighter1, fighter2] = fighters;
    const [currentDamageFighter1, setCurrentDamageFighter1] = useState(0);
    const [currentDamageFighter2, setCurrentDamageFighter2] = useState(0);


    let player1SumDamage = 0;
    let player2SumDamage = 0;

    let isPlayerOneBlockActive = false;
    let isPlayerTwoBlockActive = false;

    const handleKeyDown = (event) => {
        const {code} = event;

        switch (code) {
            case PlayerOneBlock:
                isPlayerOneBlockActive = true;
                break;
            case PlayerTwoBlock:
                isPlayerTwoBlockActive = true;
                break;
            case PlayerOneAttack:
                const FIGHTER_ONE_CAN_HIT = !isPlayerTwoBlockActive && !isPlayerOneBlockActive;
                if (FIGHTER_ONE_CAN_HIT) {
                    const damage = getDamage(fighter1, fighter2);
                    player2SumDamage += damage;
                    setCurrentDamageFighter2(player2SumDamage);
                }
                break;
            case PlayerTwoAttack:
                const FIGHTER_TWO_CAN_HIT = !isPlayerTwoBlockActive && !isPlayerOneBlockActive;
                if (FIGHTER_TWO_CAN_HIT) {
                    const damage = getDamage(fighter1, fighter2);
                    player1SumDamage += damage;
                    setCurrentDamageFighter1(player1SumDamage);
                }
                break;
            default:
        }

    };

    const handleKeyUp = (event) => {
        const {code} = event;

        switch (code) {
            case PlayerOneBlock:
                isPlayerOneBlockActive = false;
                break;
            case PlayerTwoBlock:
                isPlayerTwoBlockActive = false;
                break;
            default:

        }
    };

    useEffect(() => {
        document.addEventListener('keydown', handleKeyDown, false);
        document.addEventListener('keyup', handleKeyUp, false);

        // cleanup this component
        return () => {
            document.removeEventListener('keydown', handleKeyDown, false);
            document.removeEventListener('keyup', handleKeyUp, false);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="arena">
            <FightStatus
                fighters={fighters}
                currentDamageFighter1={currentDamageFighter1}
                currentDamageFighter2={currentDamageFighter2}
                setWinner={setWinner}
            />
            <BattleField/>
        </div>
    );
};

export default React.memo(Arena);

