import React from 'react';

import './healthIndicator.css';
import HealthBar from "../healthBar";

const HealthIndicator = ({fighter: {health}, currentDamageHealth}) => {

    return (
        <div className="health-indicator">
            <HealthBar progress={100 - (currentDamageHealth / health * 100)}/>
        </div>
    );
};

export default HealthIndicator;
