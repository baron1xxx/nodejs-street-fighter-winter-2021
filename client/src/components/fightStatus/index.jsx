import React, {useEffect} from 'react';
import FighterIndicator from "../fighterIndicator";

import './fightStatus.css';

const FightStatus = ({fighters: [fighter1, fighter2], currentDamageFighter1, currentDamageFighter2, setWinner}) => {
    useEffect(() => {
        if (currentDamageFighter2 >= fighter2.health) {
            setWinner(fighter1);
        }
        if (currentDamageFighter1 >= fighter1.health) {
            setWinner(fighter2);
        }
        // eslint-disable-next-line
    },[currentDamageFighter1, currentDamageFighter2]);

    return (
        <div className="fight-status">
            <FighterIndicator fighter={fighter1} currentDamageHealth={currentDamageFighter1} />
            <FighterIndicator fighter={fighter2} currentDamageHealth={currentDamageFighter2} />
        </div>
    );
};

export default FightStatus;
