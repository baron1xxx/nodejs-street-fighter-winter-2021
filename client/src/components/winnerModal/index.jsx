import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {Modal} from '@material-ui/core';

import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        width: 400,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textCenter: {
        textAlign: 'center'
    },
    media: {
        height: 140,
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }
});

const WinnerModal = ({winner, setNewGame}) => {
    const classes = useStyles();
    const onStartNewGame = () => {
        setNewGame();
    };

    return (
        <div>
            <Modal className={classes.modal} open={true}>
                <Card className={classes.root}>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image="https://d3fa68hw0m2vcc.cloudfront.net/e2d/110564923.jpeg"
                            title="Contemplative Reptile"
                        />
                        <CardContent>
                            <Typography className={classes.textCenter} gutterBottom variant="h5" component="h2">
                                {winner?.name || 'Ivan'}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions className={classes.root}>
                        <Button
                            size="small"
                            color="primary"
                            variant="contained"
                            onClick={onStartNewGame}
                        >
                            New game
                        </Button>
                    </CardActions>
                </Card>
            </Modal>
        </div>
    );
};

export default WinnerModal;
