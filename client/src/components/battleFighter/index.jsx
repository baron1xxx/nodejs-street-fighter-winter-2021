import React from 'react';

import './battleFighter.css';

const BattleFighter = ({fighterImg, position}) => {
    return (
        <div className="battleFighter" id={`${position}Fighter`}>
            <img src={fighterImg} alt="Fighter" />
        </div>
    );
};

export default BattleFighter;
