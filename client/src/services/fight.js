import {MAX_CRITICAL_HIT, MIN_CRITICAL_HIT} from '../constants/hitPower';

export function getDamage(attacker, defender) {
    const damage = getHitPower(attacker) - getBlockPower(defender);
    return Math.max(0, damage);
}

export function getHitPower(fighter) {
    return fighter.power * (Math.random() * (MAX_CRITICAL_HIT - MIN_CRITICAL_HIT) + MIN_CRITICAL_HIT);
}

export function getBlockPower(fighter) {
    return fighter.defense * (Math.random() * (MAX_CRITICAL_HIT - MIN_CRITICAL_HIT) + MIN_CRITICAL_HIT);
}

export function getCriticalHitPower(fighter) {
    return fighter.attack * 2;
}
