const Joi = require('joi');
const regExpExpressions = require('../constants/regExpExpressions');
const { lessThan } = require('./customValidators');


module.exports = Joi.object({
    name: Joi
        .string()
        .trim()
        .required()
        .min(3)
        .max(20)
        .regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'name must be string',
            'string.min': 'minimum {#limit} symbol ',
            'string.max': 'maximum {#limit} symbol ',
            'string.pattern.base': 'only letters.',
            'string.required': 'name is required'
        }),
    health: Joi
        .number()
        .integer()
        .positive()
        .required()
        .min(0)
        .max(100)
        .messages({
            'string.base': 'health must be number',
            'string.integer': 'health must be integer number',
            'string.positive': 'health must be positive number',
            'string.required': 'health is required',
            'string.min': 'minimum {#limit}.',
            'string.max': 'maximum {#limit}.'
        }),
    power: Joi
        .number()
        .integer()
        .positive()
        .required()
        .min(0)
        .custom(lessThan)
        .messages({
            'string.base': 'power must be number',
            'string.integer': 'power must be integer number',
            'string.positive': 'power must be positive number',
            'string.required': 'health is required',
            'string.min': 'minimum {#limit}.',
            'string.max': 'maximum {#limit}.'
        }),

    defense: Joi
        .number()
        .integer()
        .positive()
        .required()
        .min(1)
        .max(10)
        .messages({
            'string.base': 'defense must be number',
            'string.integer': 'defense must be integer number',
            'string.positive': 'defense must be positive number',
            'string.required': 'health is required',
            'string.min': 'minimum {#limit}.',
            'string.max': 'maximum {#limit}.'
        })
});
