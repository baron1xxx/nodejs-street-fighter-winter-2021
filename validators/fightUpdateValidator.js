const Joi = require('joi');

module.exports = Joi.object().keys({
    fighter1: Joi
        .string()
        .required()
        .guid({version: ['uuidv4']})
        .messages({
            'string.base': 'ID must be string',
            'string.required': 'ID is required'
        }),
    fighter2: Joi
        .string()
        .required()
        .guid({version: ['uuidv4']})
        .messages({
            'string.base': 'ID must be string',
            'string.required': 'ID is required'
        }),
    log: Joi.array().items(
        Joi.object({
            fighter1Health: Joi.number().required().positive().min(0).max(100),
            fighter2Health: Joi.number().required().positive().min(0).max(100),
        })
    )
});
