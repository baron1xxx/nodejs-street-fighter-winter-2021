const lessThan = (value, helpers) => value < 100 ? value : helpers.message('Power must be less than 100');

module.exports = {
    lessThan
};
