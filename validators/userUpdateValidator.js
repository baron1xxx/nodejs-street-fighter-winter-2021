const Joi = require('joi');
const regExpExpressions = require('../constants/regExpExpressions');

module.exports = Joi.object({
    firstName: Joi
        .string()
        .trim()
        .min(3)
        .max(20)
        .regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'FirstName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.',
        }),
    lastName: Joi
        .string()
        .trim()
        .min(3)
        .max(20)
        .regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'LastName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.',
        }),
    email: Joi
        .string()
        .trim()
        .regex(new RegExp(regExpExpressions.email))
        .messages({
            'string.base': 'Email must be string',
            'string.pattern.base': 'Email pattern ivan@ivan.com'
        }),
    phoneNumber: Joi
        .string()
        .trim()
        .regex(new RegExp(regExpExpressions.phoneNumber))
        .messages({
            'string.base': 'Phone number must be string',
            'string.pattern.base': 'Phone number pattern +380xxxxxxxxx'
        }),
    password: Joi
        .string()
        .min(3)
        .regex(new RegExp(regExpExpressions.password))
        .messages({
            'string.base': 'Password must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.pattern.base': 'Password doesn\'t match the pattern'
        }),
});
