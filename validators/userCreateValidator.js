const Joi = require('joi');
const regExpExpressions = require('../constants/regExpExpressions');

module.exports = Joi.object({
    firstName: Joi
        .string()
        .trim()
        .required()
        .min(3)
        .max(20)
        .regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'FirstName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.',
            'string.required': 'FirstName is required'
        }),
    lastName: Joi
        .string()
        .trim()
        .required()
        .min(3)
        .max(20)
        .regex(new RegExp(regExpExpressions.name))
        .messages({
            'string.base': 'LastName must be string',
            'string.min': 'Minimum {#limit} symbol ',
            'string.max': 'Maximum {#limit} symbol ',
            'string.pattern.base': 'Only letters.',
            'string.required': 'LastName is required'
        }),
    email: Joi
        .string()
        .trim()
        .required()
        .regex(new RegExp(regExpExpressions.email))
        .messages({
            'string.base': 'Email must be string',
            'string.required': 'Email is required',
            'string.pattern.base': 'Email pattern ivan@gmail.com'
        }),
    phoneNumber: Joi
        .string()
        .trim()
        .required()
        .regex(new RegExp(regExpExpressions.phoneNumber))
        .messages({
            'string.base': 'Phone number must be string',
            'string.required': 'Phone number is required',
            'string.pattern.base': 'Phone number pattern +380xxxxxxxxx'
        }),
    password: Joi
        .string()
        .required()
        .min(3)
        .regex(new RegExp(regExpExpressions.password))
        .messages({
            'string.base': 'Password must be string',
            'string.required': 'Password is required',
            'string.min': 'Minimum {#limit} symbol ',
            'string.pattern.base': 'Password doesn\'t match the pattern'
        }),
});
