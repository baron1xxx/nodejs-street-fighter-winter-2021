const {fighter} = require('../models/fighter');
const ErrorHandler = require('../error/ErrorHandler');
const fightCreateValidator = require('../validators/fightCreateValidator');
const fightUpdateValidator = require('../validators/fightUpdateValidator');
const {BEAD_REQUEST} = require('../constants/responseStatusCodes');

const createFightValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during creation
    try {
        const {error, value: fightData} = fightCreateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandler(` ${error.details[0].path[0]} ${error.details[0].message}`, BEAD_REQUEST));
        }
        req.body = fightData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }

};

const updateFightValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during update
    try {
        const {error, value: fightData} = fightUpdateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandler(` ${error.details[0].path[0]} ${error.details[0].message}`, BEAD_REQUEST));
        }
        req.body = fightData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }

};

exports.createFightValid = createFightValid;
exports.updateFightValid = updateFightValid;
