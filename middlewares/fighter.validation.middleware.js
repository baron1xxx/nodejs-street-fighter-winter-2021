const {fighter} = require('../models/fighter');
const ErrorHandler = require('../error/ErrorHandler');
const fighterCreateValidator = require('../validators/fighterCreateValidator');
const fighterUpdateValidator = require('../validators/fighterUpdateValidator');
const {BEAD_REQUEST} = require('../constants/responseStatusCodes');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during creation
    try {
        const {error, value: fighterData} = fighterCreateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandler(` ${error.details[0].path[0]} ${error.details[0].message}`, BEAD_REQUEST));
        }
        req.body = fighterData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }

};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during update
    try {
        const {error, value: fighterData} = fighterUpdateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandler(` ${error.details[0].path[0]} ${error.details[0].message}`, BEAD_REQUEST));
        }
        req.body = fighterData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }

};

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
