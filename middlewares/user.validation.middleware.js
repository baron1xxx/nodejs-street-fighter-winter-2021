const {user} = require('../models/user');
const ErrorHandler = require('../error/ErrorHandler');
const userCreateValidator = require('../validators/userCreateValidator');
const userUpdateValidator = require('../validators/userUpdateValidator');
const {BEAD_REQUEST} = require('../constants/responseStatusCodes');

const createUserValid = (req, res, next) => {
    // TODO: Implement validator for user entity during creation
    try {
        const {error, value: userData} = userCreateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandler(` ${error.details[0].path[0]} ${error.details[0].message}`, BEAD_REQUEST));
        }
        req.body = userData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }

};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validator for user entity during update
    try {
        const {error, value: userData} = userUpdateValidator.validate(req.body);
        if (error) {
            return next(new ErrorHandler(` ${error.details[0].path[0]} ${error.details[0].message}`, BEAD_REQUEST));
        }
        req.body = userData;
        next();
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
