const ErrorHandler = require('./../error/ErrorHandler');
const { OK } = require('./../constants/responseStatusCodes');

const responseMiddleware= (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    try {
        const { err, data } = req;
        return err
            ? next(new ErrorHandler(err.message, err.status))
            : res.status(OK).json(data);
    } catch (e) {
        next(new ErrorHandler(e.message, e.status));
    }
};

exports.responseMiddleware = responseMiddleware;
