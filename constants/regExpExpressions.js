module.exports = {
    name: '^[a-zA-ZА-ЩЬЮЯҐЄІЇа-щьюяґєії]+$',
    email: '[a-zA-Z0-9]+[\\w.+\\-]+[a-zA-Z0-9]+@gmail\\.com+$',
    password: '^[a-zA-Z0-9]{3,20}$',
    phoneNumber: '^(\\+38)(\\d{10})$'
};
