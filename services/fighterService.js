const { FighterRepository } = require('../repositories/fighterRepository');
const ErrorHandler = require('./../error/ErrorHandler');
const {
    BEAD_REQUEST,
    NOT_FOUND
} = require('./../constants/responseStatusCodes');
const {
    FIGHTERS_NOT_FOUND,
    FIGHTER_NOT_FOUND,
    FIGHTER_NOT_CREATED,
    FIGHTER_NOT_UPDATED,
    FIGHTER_NOT_DELETED,
    FIGHTER_NAME_EXITS
} = require('./../constants/responseErrorMessages');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll() {
        const fighters = FighterRepository.getAll();
        if (Array.isArray(fighters) && !fighters.length) {
            throw new ErrorHandler(FIGHTERS_NOT_FOUND, NOT_FOUND);
        }
        return fighters;
    }

    getById(id) {
        const fighter = this.search({id});
        if (!fighter) {
            throw new ErrorHandler(FIGHTER_NOT_FOUND, NOT_FOUND);
        }
        return fighter;
    }


    create(data) {
        const {name} = data;
        this.checkIfExistsByName(name);
        const fighter = FighterRepository.create(data);
        if (!fighter) {
            throw new ErrorHandler(FIGHTER_NOT_CREATED, BEAD_REQUEST);
        }
        return fighter;
    }

    update(id, data) {
        const fighterExist = this.getById(id);
        const {name} = data;
        if (name && name !== fighterExist.name) {
            this.checkIfExistsByName(name);
        }
        const fighter = FighterRepository.update(id, data);
        if (!fighter || !Object.keys(fighter).length) {
            throw new ErrorHandler(FIGHTER_NOT_UPDATED, BEAD_REQUEST);
        }
        return fighter;
    }

    delete(id) {
        this.getById(id);
        const fighter = FighterRepository.delete(id);
        if (Array.isArray(fighter) && !fighter.length) {
            throw new ErrorHandler(FIGHTER_NOT_DELETED, BEAD_REQUEST);
        }
        return fighter;
    }

    search(search) {
        const fighter = FighterRepository.getOne(search);
        if (!fighter) {
            return null;
        }
        return fighter;
    }

    checkIfExistsByName(name) {
        const nameExists = this.search({name});
        if (nameExists) {
            throw new ErrorHandler(FIGHTER_NAME_EXITS, BEAD_REQUEST);
        }
    }

}

module.exports = new FighterService();
