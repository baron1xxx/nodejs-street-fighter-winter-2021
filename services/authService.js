const UserService = require('./userService');
const ErrorHandler = require('./../error/ErrorHandler');
const {UNAUTHORIZED} = require('./../constants/responseStatusCodes');
const {USER_NOT_FOUND} = require('./../constants/responseErrorMessages');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if (!user) {
            throw new ErrorHandler(USER_NOT_FOUND, UNAUTHORIZED);
        }
        return user;
    }
}

module.exports = new AuthService();
