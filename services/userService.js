const {UserRepository} = require('../repositories/userRepository');
const ErrorHandler = require('./../error/ErrorHandler');
const {
    BEAD_REQUEST,
    NOT_FOUND
} = require('./../constants/responseStatusCodes');
const {
    EMAIL_EXITS,
    PHONE_NUMBER_EXITS,
    USER_NOT_FOUND,
    USER_NOT_CREATED,
    USER_NOT_UPDATED,
    USER_NOT_DELETED,
    USERS_NOT_FOUND,
} = require('./../constants/responseErrorMessages');

class UserService {

    // TODO: Implement methods to work with user
    getAll() {
        const users = UserRepository.getAll();
        if (Array.isArray(users) && !users.length) {
            throw new ErrorHandler(USERS_NOT_FOUND, NOT_FOUND);
        }
        return users;
    }

    getById(id) {
        const user = this.search({id});
        if (!user) {
            throw new ErrorHandler(USER_NOT_FOUND, NOT_FOUND);
        }
        return user;
    }


    create(data) {
        const {email, phoneNumber} = data;
        this.checkIfExistsByEmail(email);
        this.checkIfExistsByPhoneNumber(phoneNumber);
        const user = UserRepository.create(data);
        if (!user) {
            throw new ErrorHandler(USER_NOT_CREATED, BEAD_REQUEST);
        }
        return user;
    }

    update(id, data) {
        const userExist = this.getById(id);
        const {email, phoneNumber} = data;
        if (email && email !== userExist.email) {
            this.checkIfExistsByEmail(email);
        }
        if (phoneNumber && phoneNumber !== userExist.phoneNumber) {
            this.checkIfExistsByPhoneNumber(phoneNumber);
        }
        const user = UserRepository.update(id, data);
        if (!user || !Object.keys(user).length) {
            throw new ErrorHandler(USER_NOT_UPDATED, BEAD_REQUEST);
        }
        return user;
    }

    delete(id) {
        this.getById(id);
        const user = UserRepository.delete(id);
        if (Array.isArray(user) && !user.length) {
            throw new ErrorHandler(USER_NOT_DELETED, BEAD_REQUEST);
        }
        return user;
    }

    search(search) {
        const user = UserRepository.getOne(search);
        if (!user) {
            return null;
        }
        return user;
    }

    checkIfExistsByEmail(email) {
        const emailExists = this.search({email});
        if (emailExists) {
            throw new ErrorHandler(EMAIL_EXITS, BEAD_REQUEST);
        }
    }

    checkIfExistsByPhoneNumber(phoneNumber) {
        const phoneNumberExists = this.search({phoneNumber});
        if (phoneNumberExists) {
            throw new ErrorHandler(PHONE_NUMBER_EXITS, BEAD_REQUEST);
        }
    }
}

module.exports = new UserService();
