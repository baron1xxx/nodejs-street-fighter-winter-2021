const { FightRepository } = require('../repositories/fightRepository');
const ErrorHandler = require('./../error/ErrorHandler');
const {
    BEAD_REQUEST,
    NOT_FOUND
} = require('./../constants/responseStatusCodes');
const {
    FIGHT_NOT_FOUND,
    FIGHTS_NOT_FOUND,
    FIGHT_NOT_CREATED,
    FIGHT_NOT_UPDATED,
    FIGHT_NOT_DELETED
} = require('./../constants/responseErrorMessages');

class FightService {
    // OPTIONAL TODO: Implement methods to work with fights
    getAll() {
        const fights = FightRepository.getAll();
        if (Array.isArray(fights) && !fights.length) {
            throw new ErrorHandler(FIGHTS_NOT_FOUND, NOT_FOUND);
        }
        return fights;
    }

    getById(id) {
        const fight = this.search({id});
        if (!fight) {
            throw new ErrorHandler(FIGHT_NOT_FOUND, NOT_FOUND);
        }
        return fight;
    }


    create(data) {
        const fight = FightRepository.create(data);
        if (!fight) {
            throw new ErrorHandler(FIGHT_NOT_CREATED, BEAD_REQUEST);
        }
        return fight;
    }

    update(id, data) {
        const fightExist = this.getById(id);
        const fight = FightRepository.update(id, data);
        if (!fight || !Object.keys(fight).length) {
            throw new ErrorHandler(FIGHT_NOT_UPDATED, BEAD_REQUEST);
        }
        return fight;
    }

    delete(id) {
        this.getById(id);
        const fight = FightRepository.delete(id);
        if (Array.isArray(fight) && !fight.length) {
            throw new ErrorHandler(FIGHT_NOT_DELETED, BEAD_REQUEST);
        }
        return fight;
    }

    search(search) {
        const fight = FightRepository.getOne(search);
        if (!fight) {
            return null;
        }
        return fight;
    }
}

module.exports = new FightService();
