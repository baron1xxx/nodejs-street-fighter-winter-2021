const {Router} = require('express');
const AuthService = require('../services/authService');
// const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login',  (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const { body: userData } = req;
        req.data = AuthService.login(userData);
    } catch (err) {
        req.err = err;
    } finally {
        next();
    }
});

module.exports = router;
