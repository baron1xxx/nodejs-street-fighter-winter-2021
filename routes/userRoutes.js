const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
// const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router
    .get('/', (req, res, next) => {
        try {
            console.log('GET ALL WORK!!!');
            req.data = UserService.getAll();
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .get('/:id', (req, res, next) => {
        try {
            console.log('GET BY ID WORK!!!');
            req.data = UserService.getById(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .post('/', createUserValid, (req, res, next) => {
        try {
            console.log('CREATE WORK!!!');
            // TODO: Implement login action (get the user if it exist with entered credentials)
            const {body: userData} = req;
            req.data = UserService.create(userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .put('/:id', updateUserValid, (req, res, next) => {
        try {
            console.log('UPDATE WORK!!!');
            const {body: userData, params: {id}} = req;
            req.data = UserService.update(id, userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .delete('/:id', (req, res, next) => {
        try {
            console.log('DELETE WORK!!!');
            req.data = UserService.delete(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    });

module.exports = router;
