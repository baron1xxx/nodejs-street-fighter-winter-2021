const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router
    .get('/', (req, res, next) => {
        try {
            console.log('GET ALL FIGHT WORK!!!');
            req.data = FightService.getAll();
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .get('/:id', (req, res, next) => {
        try {
            console.log('GET FIGHT BY ID WORK!!!');
            req.data = FightService.getById(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .post('/', createFightValid, (req, res, next) => {
        try {
            console.log('CREATE FIGHT WORK!!!');
            // TODO: Implement login action (get the user if it exist with entered credentials)
            const {body: userData} = req;
            req.data = FightService.create(userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .put('/:id', updateFightValid, (req, res, next) => {
        try {
            console.log('UPDATE FIGHT WORK!!!');
            const {body: userData, params: {id}} = req;
            req.data = FightService.update(id, userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .delete('/:id', (req, res, next) => {
        try {
            console.log('DELETE FIGHT WORK!!!');
            req.data = FightService.delete(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    });

module.exports = router;
