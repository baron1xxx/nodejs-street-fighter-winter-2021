const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router
    .get('/', (req, res, next) => {
        try {
            console.log('GET ALL FIGHTER WORK!!!');
            req.data = FighterService.getAll();
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .get('/:id', (req, res, next) => {
        try {
            console.log('GET FIGHTER BY ID WORK!!!');
            req.data = FighterService.getById(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .post('/', createFighterValid, (req, res, next) => {
        try {
            console.log('CREATE FIGHTER WORK!!!');
            // TODO: Implement login action (get the user if it exist with entered credentials)
            const {body: userData} = req;
            req.data = FighterService.create(userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .put('/:id', updateFighterValid, (req, res, next) => {
        try {
            console.log('UPDATE FIGHTER WORK!!!');
            const {body: userData, params: {id}} = req;
            req.data = FighterService.update(id, userData);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    })
    .delete('/:id', (req, res, next) => {
        try {
            console.log('DELETE FIGHTER WORK!!!');
            req.data = FighterService.delete(req.params.id);
        } catch (err) {
            req.err = err;
        } finally {
            next();
        }
    });

module.exports = router;
